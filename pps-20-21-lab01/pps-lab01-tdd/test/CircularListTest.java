import lab01.tdd.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private static final int TEST_ITERATIONS = 100;
    private CircularList circularList;
    private StrategyFactory strategyFactory;

    @BeforeEach
    void beforeEach(){
        //ARRANGE
        this.circularList = new CircularListImpl();
        this.strategyFactory = new StrategyFactoryImpl();
    }

    @Test
    void testIsEmpty(){
        //ASSERT
        assertEquals(true, circularList.isEmpty());
    }

    @Test
    void testSize(){
        //ACT
        assertEquals(0, circularList.size());
        for (int i = 0; i < TEST_ITERATIONS; i++){
            circularList.add(5);
            assertEquals(i+1, circularList.size());
        }
    }

    @Test
    void testAdd(){
        //ACT
        this.circularList.add(5);
        //ASSERT
        assertEquals(false, circularList.isEmpty());
    }

    @Test
    void testNextEmpty(){
        assertEquals(Optional.empty(), circularList.next());
    }

    @Test
    void testNext(){
        circularList.add(5);
        assertEquals(5, circularList.next().get());
    }

    @Test
    void testNextMultipleTimes(){
        circularList.add(5);
        for (int i = 0; i < TEST_ITERATIONS; i ++){
            assertEquals(5, circularList.next().get());
        }
    }

    @Test
    void testNextOrder(){
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        assertEquals(1, circularList.next().get());
        assertEquals(2, circularList.next().get());
        assertEquals(3, circularList.next().get());

    }

    @Test
    void testPrevious(){
        circularList.add(5);
        assertEquals(5, circularList.previous().get());
    }

    @Test
    void testPreviousMultipleTimes(){
        circularList.add(5);
        for (int i = 0; i < TEST_ITERATIONS; i ++){
            assertEquals(5, circularList.previous().get());
        }
    }

    @Test
    void testPreviousOrder(){
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        assertEquals(3, circularList.previous().get());
        assertEquals(2, circularList.previous().get());
        assertEquals(1, circularList.previous().get());
    }

    @Test
    void testPreviousAndNext(){
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        assertEquals(1, circularList.next().get());
        assertEquals(2, circularList.next().get());
        assertEquals(2, circularList.previous().get());
        assertEquals(1, circularList.previous().get());
    }


    @Test
    void testReset(){
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        circularList.next(); //1st element
        circularList.next(); //2nd element
        circularList.reset();
        assertEquals(2, circularList.next().get()); //3rd element
        assertEquals(3, circularList.next().get()); //1st element
        assertEquals(1, circularList.next().get()); //2nd element

    }

    @Test
    void testNextWithEvenStrategy(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(2, circularList.next(strategyFactory.createEvenStrategy()).get());
    }

    @Test
    void testNextWithMultipleOfStrategy(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(2, circularList.next(strategyFactory.createMultipleOfStrategy(2)).get());
    }

    @Test
    void testNextWithEqualsStrategy(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(2, circularList.next(strategyFactory.createEqualsStrategy(2)).get());
    }

    @Test
    void testNextWithStrategyEmpty(){
        circularList.add(1);
        circularList.add(2);
        assertEquals(Optional.empty(), circularList.next(strategyFactory.createEqualsStrategy(3)));
    }

}
