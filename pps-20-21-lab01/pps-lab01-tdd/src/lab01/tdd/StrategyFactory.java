package lab01.tdd;

public interface StrategyFactory {

    SelectStrategy createEvenStrategy();

    SelectStrategy createMultipleOfStrategy(int value);

    SelectStrategy createEqualsStrategy(int value);

}
