package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {

    private List<Integer> circularList;
    private int index;

    public CircularListImpl (){
        circularList = new ArrayList<>();
        index = 0;
    }

    @Override
    public void add(int element) {
        this.circularList.add(element);
    }

    @Override
    public int size() {
        return this.circularList.size();
    }

    @Override
    public boolean isEmpty() {
        return circularList.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if(isEmpty()){
            return Optional.empty();
        }

        if (circularList.size() == index){
            index = 0;
        }

        return Optional.of(circularList.get(index++));
    }

    @Override
    public Optional<Integer> previous() {
        if (isEmpty()) return Optional.empty();

        if (index == 0){
            index = circularList.size();
        }

        return Optional.of(circularList.get(--index));
    }

    @Override
    public void reset() {
        if (!isEmpty()){
            int actual = circularList.remove(index);
            List<Integer> newList = new ArrayList<>();
            newList.add(actual);
            for (Integer value: circularList) {
                newList.add(value);
            }
            this.circularList = newList;
        }

    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        if (isEmpty()) return Optional.empty();

        boolean strategyResult = false;
        int initialIndex = this.index; // Utilizzato per capire quando ho fatto un giro intero della lista
        while (strategyResult == false ){
            strategyResult = strategy.apply(next().get());
            System.out.println("initial: " + initialIndex + ", index: " + index);
            if (initialIndex == index-1) {
                break;
            }
        }

        if (strategyResult == true){
            return previous();
        }
        return Optional.empty();
    }
}
