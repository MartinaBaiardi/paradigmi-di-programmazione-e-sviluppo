package lab01.tdd;

public class MultipleOfStrategy implements SelectStrategy{

    private int value;

    public MultipleOfStrategy(int value){
        this.value = value;
    }

    @Override
    public boolean apply(int element) {
        return element % value == 0;
    }
}
