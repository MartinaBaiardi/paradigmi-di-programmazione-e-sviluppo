package lab01.tdd;

public class EqualsStrategy implements SelectStrategy{

    private int value;

    public EqualsStrategy(int value){
        this.value = value;
    }

    @Override
    public boolean apply(int element) {
        return element == value;
    }
}
