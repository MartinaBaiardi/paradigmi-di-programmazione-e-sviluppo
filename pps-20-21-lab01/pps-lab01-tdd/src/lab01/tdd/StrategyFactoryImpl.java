package lab01.tdd;

public class StrategyFactoryImpl implements StrategyFactory {

    @Override
    public SelectStrategy createEvenStrategy() {
        return new EvenStrategy();
    }

    @Override
    public SelectStrategy createMultipleOfStrategy(int value) {
        return new MultipleOfStrategy(value);
    }

    @Override
    public SelectStrategy createEqualsStrategy(int value) {
        return new EqualsStrategy(value);
    }
}
