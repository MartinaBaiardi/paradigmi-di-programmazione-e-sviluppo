import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.SimpleBankAccount;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the SimpleBankAccount implementation
 */
class SimpleBankAccountTest extends SimpleBankAccountAbstractTest {

    private BankAccount bankAccount;

    @Override
    public BankAccount getBankAccount() {
        return bankAccount;
    }

    @BeforeEach
    @Override
    void beforeEach() {
        super.beforeEach();
        this.bankAccount = new SimpleBankAccount(getAccountHolder(), 0);
    }
}
