import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.BankAccountWithAtm;
import lab01.example.model.SimpleBankAccountWithAtm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SimpleBankAccountWithAtmTest extends SimpleBankAccountAbstractTest {

    private BankAccountWithAtm bankAccountWithAtm;

    @Override
    public BankAccount getBankAccount() {
        return this.bankAccountWithAtm;
    }

    @BeforeEach
    @Override
    void beforeEach(){
        super.beforeEach();
        this.bankAccountWithAtm = new SimpleBankAccountWithAtm(getAccountHolder(), 0);
    }

    @Test
    void testDepositWithAtm() {
        this.bankAccountWithAtm.depositWithAtm(getAccountHolder().getId(), 100);
        assertEquals(100 - SimpleBankAccountWithAtm.BANK_FEE, this.bankAccountWithAtm.getBalance());
    }

    @Test
    void testWrongDepositWithAtm() {
        this.bankAccountWithAtm.depositWithAtm(getAccountHolder().getId(), 100);
        this.bankAccountWithAtm.depositWithAtm(2, 50);
        assertEquals(100 - SimpleBankAccountWithAtm.BANK_FEE, this.bankAccountWithAtm.getBalance());
    }

    @Test
    void testWithdrawWithAtm() {
        this.bankAccountWithAtm.depositWithAtm(getAccountHolder().getId(), 100);
        this.bankAccountWithAtm.withdrawWithAtm(getAccountHolder().getId(), 70);
        assertEquals(30 - 2 * SimpleBankAccountWithAtm.BANK_FEE, this.bankAccountWithAtm.getBalance());
    }

    @Test
    void testWrongWithdrawWithAtm() {
        this.bankAccountWithAtm.depositWithAtm(getAccountHolder().getId(), 100);
        this.bankAccountWithAtm.withdrawWithAtm(2, 70);
        assertEquals(100 - SimpleBankAccountWithAtm.BANK_FEE, this.bankAccountWithAtm.getBalance());
    }
}