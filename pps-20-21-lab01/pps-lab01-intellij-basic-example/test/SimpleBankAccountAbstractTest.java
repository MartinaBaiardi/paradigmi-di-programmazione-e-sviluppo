import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.SimpleBankAccountWithAtm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class SimpleBankAccountAbstractTest {
    private AccountHolder accountHolder;

    public void setAccountHolder(final AccountHolder accountHolder) {
        this.accountHolder = accountHolder;
    }

    public AccountHolder getAccountHolder() {
        return accountHolder;
    }

    public abstract BankAccount getBankAccount();

    @BeforeEach
    void beforeEach(){
        this.setAccountHolder(new AccountHolder("Mario", "Rossi", 1));
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, getBankAccount().getBalance());
    }

    @Test
    void testDeposit() {
        getBankAccount().deposit(accountHolder.getId(), 100);
        assertEquals(100, getBankAccount().getBalance());
    }

    @Test
    void testWrongDeposit() {
        getBankAccount().deposit(accountHolder.getId(), 100);
        getBankAccount().deposit(2, 50);
        assertEquals(100, getBankAccount().getBalance());
    }

    @Test
    void testWithdraw() {
        getBankAccount().deposit(accountHolder.getId(), 100);
        getBankAccount().withdraw(accountHolder.getId(), 70);
        assertEquals(30, getBankAccount().getBalance());
    }

    @Test
    void testWrongWithdraw() {
        getBankAccount().deposit(accountHolder.getId(), 100);
        getBankAccount().withdraw(2, 70);
        assertEquals(100, getBankAccount().getBalance());
    }


}
