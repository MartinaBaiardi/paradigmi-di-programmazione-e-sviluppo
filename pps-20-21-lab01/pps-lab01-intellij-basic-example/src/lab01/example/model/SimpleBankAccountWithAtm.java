package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    public static final int BANK_FEE = 1;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    @Override
    public void depositWithAtm(int usrID, double amount) {
        this.deposit(usrID, amount - BANK_FEE);
    }

    @Override
    public void withdrawWithAtm(int usrID, double amount) {
        this.withdraw(usrID, amount + BANK_FEE);
    }
}
