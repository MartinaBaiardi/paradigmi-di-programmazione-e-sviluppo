package exercises

import u03.Lists._
import List._
import exercises.task2.Person
import exercises.task2.Person._
import Task1._

object task2 {

  sealed trait Person
  object Person {

    case class Student(name: String, year: Int) extends Person

    case class Teacher(name: String, course: String) extends Person

    def name(p: Person): String = p match {
      case Student(n, _) => n
      case Teacher(n, _) => n
    }

    def getCourses(l: List[Person]): List[String] = map(filter(l)(i => i match {
      case Teacher(n, c) => true
      case _ => false
    }))(el => el match {
      case Teacher(_,c) => c
    })

    def getCourses2(l: List[Person]): List[String] = flatMap(l)(i => i match {
      case Teacher(n, c) => Cons(c, Nil())
      case _ => Nil()
    })

    def foldLeft[A, B <: A](l: List[A])(init: B)(accumulator: (A, A) => B): B = l match {
      case Cons(h, t) => foldLeft(t)(accumulator(init, h))(accumulator)
      case _ => init
    }

    def foldRight[A, B <: A](l: List[A])(init: B)(accumulator: (A, A) => B): B = l match {
      case Cons(h, t) => accumulator(h, foldRight(t)(init)(accumulator))
      case _ => init
    }

  }
}


