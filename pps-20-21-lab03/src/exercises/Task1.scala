package exercises

import u03.Lists._
import List._
import u02.Optionals._
import Option._

object Task1 {

  def drop[A](l: List[A], n: Int): List[A] = l match {
    case Cons(_, t) if n != 0 => drop(t, n - 1)
    case _ => l
  }

  def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match {
    case Cons(h, t) => append(f(h), flatMap(t)(f))
    case _ => Nil()
  }

  def map2[A,B](l: List[A])(mapper: A=>B): List[B] = flatMap(l)(i => Cons(mapper(i), Nil()))

  def filter2[A](l1: List[A])(pred: A=>Boolean): List[A] = flatMap(l1)(i => pred(i) match {
    case true => Cons(i ,Nil())
    case _ => Nil()
  })

  def max(l: List[Int]): Option[Int] = {
    def _max(l:List[Int], elem: Option[Int]): Option[Int] = l match {
      case Cons(h,t) if h>getOrElse(elem,Int.MinValue)  => _max(t, Some(h))
      case Cons(h,t) => _max(t, elem)
      case _ => elem
    }
    _max(l, None())
  }

  def max2(l: List[Int]): Option[Int] = {
    def _max2(l:List[Int], elem: Option[Int]): Option[Int] = (l, elem) match {
      case (Cons(head, tail), Some(n)) if n > head => _max2(tail, Some(n))
      case (Cons(head, tail), _) => _max2(tail, Some(head))
      case (Nil(), elem) => elem
    }
    _max2(l, None())
  }


}
