package exercises

import u03.Streams.Stream
import u03.Streams.Stream._


object Task3 {

  def drop[A](stream: Stream[A])(n: Int): Stream[A] = stream match {
    case Cons(head, tail) if n>0 => drop(tail())(n-1)
    case _ => stream
  }

  def constant[A](value: A): Stream[A] = iterate(value)(c=>c)

  val fibs: Stream[Int] = {
    def _fib(a: Int, b: Int): Stream[Int] = cons(a, _fib(b, a+b))
    _fib(0,1)
  }
}
