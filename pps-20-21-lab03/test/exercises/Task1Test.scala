package exercises

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u03.Lists.List._
import u02.Optionals._

class Task1Test {
  import exercises.Task1._

  val list = Cons(10, Cons(20, Cons(30, Nil())))

  @Test def testListDrop(): Unit = {
    assertEquals(Cons(20, Cons(30, Nil())), drop(list, 1))
    assertEquals(Cons(30, Nil()), drop(list, 2))
    assertEquals(Nil(), drop(list, 5))
  }

  @Test def testListFlatMap(): Unit = {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(list)(v => Cons(v+1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))), flatMap(list)(v => Cons(v+1, Cons(v+2, Nil()))))
  }

  @Test def testMap2(): Unit = {
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), map2(list)(_ + 1))
  }

  @Test def testFilter2(): Unit = {
    assertEquals(Cons(10, Cons(20, Nil())), filter2(list)(_ < 25))
    assertEquals(Cons(10, Nil()), filter2(list)(_ < 20))
    assertEquals(Cons(10, Nil()), filter2(list)(_ == 10))
  }

  @Test def testMax(): Unit = {
    assertEquals(Option.Some(30), max(list))
    assertEquals(Option.None(), max(Nil()))

    val l2 = Cons(-10, Cons(-20, Cons(-30, Nil())))
    assertEquals(Option.Some(-10), max(l2))
  }

  @Test def testMax2(): Unit = {
    assertEquals(Option.Some(30), max2(list))
    assertEquals(Option.None(), max2(Nil()))

    val l2 = Cons(-10, Cons(-20, Cons(-30, Nil())))
    assertEquals(Option.Some(-10), max2(l2))
  }

}
