package exercises

import exercises.task2.Person
import exercises.task2.Person._
import org.junit.jupiter.api.Assertions.{assertArrayEquals, assertEquals}
import org.junit.jupiter.api.Test
import u03.Lists.List.{Cons, Nil}
import u03.Streams.Stream

class Task2Test {

  @Test def testGetCourses(): Unit ={
    val list = Cons[Person](Student("anitvam", 2020), Cons[Person](Teacher("martina", "Kubernetes"), Cons[Person](Teacher("baiatina", "Something bad"), Nil[Person]())))
    assertEquals(Cons("Kubernetes", Cons("Something bad", Nil())), getCourses(list))
  }

  @Test def testGetCourses2(): Unit ={
    val list = Cons[Person](Student("anitvam", 2020), Cons[Person](Teacher("martina", "Kubernetes"), Cons[Person](Teacher("baiatina", "Something bad"), Nil[Person]())))
    assertEquals(Cons("Kubernetes", Cons("Something bad", Nil())), getCourses2(list))
  }

  val l = Cons(3, Cons(7, Cons(1, Cons( 5, Nil()))))
  @Test def testFoldLeft() = {
    assertEquals(-16, foldLeft(l)(0)(_ - _))
    assertEquals(0, foldLeft(Nil[Int]())(0)(_ + _))
  }

  @Test def testFoldRight() = {
    val s = Stream.take(Stream.iterate(0)(_ + 1))(10)
    assertEquals(-8, foldRight(l)(0)(_ - _))
  }




}
