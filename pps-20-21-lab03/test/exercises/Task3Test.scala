package exercises

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u03.Streams.Stream._
import u03.Streams._
import u03.Lists._
import exercises.Task3._


class Task3Test {


  @Test def testStreamDrop: Unit = {
    val s = Stream.take(Stream.iterate(0)(_ + 1)) (10)
    assertEquals(List.Cons(6, List.Cons(7, List.Cons(8, List.Cons(9, List.Nil())))), Stream.toList(drop(s)(6)))
  }

  @Test def testStreamConstant: Unit = {
    assertEquals(List.Cons("x", List.Cons("x", List.Cons("x", List.Cons("x", List.Cons("x", List.Nil()))))),
      Stream.toList(Stream.take(constant("x"))(5)))
  }

  @Test def testFibonacci: Unit = {
    assertEquals(List.Cons(0, List.Cons(1, List.Cons(1, List.Cons(2, List.Cons(3, List.Cons(5, List.Cons(8, List.Cons(13, List.Nil())))))))),
      Stream.toList(Stream.take(fibs)(8)))
  }

}
