package exercises

object Tasks {

  // ---------------------------------------------------------------- Exercise 3
  val parityFunction:(Int) => String = {
    case n if (n%2 == 0) => "even";
    case _ => "odd";
  }

  def parityMethod (i: Int) : String = i match{
    case n if (n%2 == 0) => "even";
    case _ => "odd";
  }

  // val negFunction:(String => Boolean) => (String => Boolean) = f => s => !f(s)
  val negFunction:(String => Boolean) => (String => Boolean) = f => !f(_)

  //def negMethod (f: String=>Boolean) : (String=>Boolean) = !f(_)
  def negMethod[A] (f: A=>Boolean): (A=>Boolean) = !f(_)

  val empty: String => Boolean = _=="";

  // ---------------------------------------------------------------- Exercise 4
  // Currying Fun Type
  def p3 (x:Int)(y:Int)(z:Int): Boolean = x<=y && y<=z;
  val p1: Int => Int => Int => Boolean = x => y => z => (x<=y && y<=z);
  def p4 (x: Int, y: Int, z: Int): Boolean = x<=y && y<=z;
  val p2: (Int, Int, Int) => Boolean =  (x,y,z) => x<=y && y<=z;

  // ---------------------------------------------------------------- Exercise 5
  //def compose (f: Int => Int, g: Int => Int): Int => Int = (i: Int) => f(g(i));
  def compose[A] (f: A=>A, g: A=>A): A=>A = i => f(g(i));

  // ---------------------------------------------------------------- Exercise 6
  def fib (n: Int): Int = n match {
    case 0 => 0;
    case 1 => 1;
    case _ => fib(n-1) + fib(n-2);
  }

  def fibTail (n: Int): Int = {
    @annotation.tailrec
    def _fib(n: Int, a: Int, b: Int) : Int = n match {
      case 0 => a;
      case 1 => b;
      case _ => {
        System.out.println(n-1 + " " + b + " " + (a+b));
        _fib(n-1, b, a+b)
      };
    }
    System.out.println(n + " " + 0 + " " + (1));
    _fib(n, 0, 1);
  }

  // ---------------------------------------------------------------- Exercise 7
  sealed trait Shape
  object Shape {
    case class Rectangle(a: Double, b: Double) extends Shape
    case class Circle(radius: Double) extends Shape
    case class Square(l: Double) extends Shape

    def perimeter (shape: Shape): Double = shape match {
      case Rectangle(a, b) => a*2+b*2
      case Circle(a) => Math.PI*2*a
      case Square(a) => a*4
    }

    def area (shape: Shape): Double = shape match {
      case Rectangle(a,b) => a*b
      case Circle(a) => Math.PI*Math.pow(a,2)
      case Square(a) => Math.pow(a,2)
    }
  }

  // ---------------------------------------------------------------- Exercise 8
  sealed trait Option[A]
  object Option {
    case class None[A]() extends Option[A]
    case class Some[A](a: A) extends Option[A]

    def isEmpty[A](opt: Option[A]): Boolean = opt match {
      case None() => true;
      case _ => false
    }

    def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
      case Some(a) => a
      case _ => orElse
    }

    def flatMap[A, B] (opt: Option[A])(f: A => Option[B]): Option[B] = opt match {
      case Some(a) => f(a)
      case _ => None()
    }

    def filter[A] (opt: Option[A])(f: A => Boolean): Option[A] = opt match {
      case Some(a) => if (f(a)) Some(a) else None()
      case _ => None()
    }

    def map[A, B] (opt: Option[A])(f: A => B): Option[B] = opt match {
      case Some(a) => Some(f(a))
      case _ => None()
    }

    def map2[A, B, C](opt1: Option[A], opt2: Option[B])(f: (A,B) => C): Option[C] = (opt1, opt2) match {
      case (Some(a), Some(b)) => Some(f(a, b))
      case _ => None()
    }

    // ---------------------------------------------------------------- Challenge
    def double[A](opt: Option[Int]): Option[Int] = map(opt)(_ * 2)

    def invert[A] (opt: Option[Boolean]): Option[Boolean] = map(opt)(!_)

  }


}
