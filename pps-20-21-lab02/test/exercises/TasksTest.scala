package exercises

import exercises.Tasks.Option._
import exercises.Tasks.Shape._
import exercises.Tasks._
import org.junit.Assert._
import org.junit.Test

class TasksTest {

  // ---------------------------------------------------------------- Exercise 3
  @Test def testParityFunction(): Unit ={
    assertEquals("even", parityFunction(2));
    assertEquals("odd", parityFunction(5));
  }

  @Test def testParityMethod(): Unit = {
    assertEquals("even", parityMethod(2));
    assertEquals("odd", parityMethod(5));
  }

  @Test def testNegFunction(): Unit ={
    fooNegTests(negFunction(empty));
  }

  @Test def testNegMethod(): Unit ={
    fooNegTests(Tasks.negMethod(empty));
  }

  private def fooNegTests(f: String=>Boolean): Unit ={
    assertEquals(true, f("foo"));
    assertEquals(false, f(""));
    assertEquals(true, f("foo") && !f(""));
  }

  val number: Integer => Boolean = _==7;

  @Test def testNegAbstract(): Unit ={
    val f = Tasks.negMethod(number);
    assertEquals(true, f(5));
    assertEquals(false, f(7));
  }

  // ---------------------------------------------------------------- Exercise 4
  @Test def testCurrying(){
    assertEquals(true, p1(1)(2)(3))
    assertEquals(true, p2(1,2,3))
    assertEquals(true, p3(1)(2)(3))
    assertEquals(true, p4(1,2,3))
    assertEquals(false, p1(1)(5)(3))
    assertEquals(false, p2(1,5,3))
    assertEquals(false, p3(1)(5)(3))
    assertEquals(false, p4(1,5,3))
  }

  // ---------------------------------------------------------------- Exercise 5
  @Test def testComposition(): Unit ={
    assertEquals(9, compose[Int](_-1, _*2)(5));
    assertEquals("provagf", compose[String](_+"f", _+"g")("prova"));
  }

  // ---------------------------------------------------------------- Exercise 6
  @Test def testFibonacci(): Unit ={
    assertEquals(0, fib(0));
    assertEquals(1, fib(1));
    assertEquals(1, fib(2));
    assertEquals(2, fib(3));
    assertEquals(3, fib(4));
    assertEquals(55, fib(10));
  }

  @Test def testFibonacciTail(): Unit ={
    assertEquals(0, fibTail(0));
    System.out.println()
    assertEquals(1, fibTail(1));
    System.out.println()
    assertEquals(1, fibTail(2));
    System.out.println()
    assertEquals(2, fibTail(3));
    System.out.println()
    assertEquals(3, fibTail(4));
    System.out.println()
    assertEquals(55, fibTail(10));
  }

  // ---------------------------------------------------------------- Exercise 7
  val a: Shape = Rectangle(2,3)
  val b: Shape = Circle(4)
  val c: Shape = Square(2)

  @Test def testShapePerimeter(): Unit = {
    assertEquals(10, perimeter(a))
    assertEquals(8*Math.PI, perimeter(b))
    assertEquals(8, perimeter(c))
  }

  @Test def testShapeArea(): Unit = {
    assertEquals(6, area(a))
    assertEquals(16*Math.PI, area(b))
    assertEquals(4, area(c))
  }

  // ---------------------------------------------------------------- Exercise 8
  @Test def testOptionFilter(): Unit ={
    assertEquals(Some(5), filter(Some(5))(_ > 2))
    assertEquals(None(), filter(Some(5))(_ > 8))
  }

  @Test def testOptionMap(): Unit = {
    assertEquals(Some(true), map(Some(5))(_ > 2))
    assertEquals(Some(false), map(Some(5))(_ > 8))
    assertEquals(None(), map(None[Int])(_ > 8))
  }

  @Test def testOptionMap2(): Unit ={
    assertEquals(Some(true), map2(Some(5), Some(8))(_ < _))
    assertEquals(Some(false), map2(Some(5), Some(2))(_ < _))
    assertEquals(None(), map2(None[Int], Some(3))(_ > _))

  }

  // ---------------------------------------------------------------- Challenge
  @Test def testDouble(): Unit ={
    assertEquals(Some(2), double(Some(1)))
    assertEquals(Some(4), double(Some(2)))
  }

  @Test def testInvert(): Unit ={
    assertEquals(Some(true), invert(Some(false)))
  }


}
