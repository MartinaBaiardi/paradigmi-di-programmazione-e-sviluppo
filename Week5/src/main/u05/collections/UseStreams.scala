package main.u05.collections

object UseStreams extends App {

  // list is an immutable linear sequence, optimising head and tail access
  val s: Stream[Int] = Stream(10,20,30,40,50) // works without imports
  println(s.getClass) // class scala.collection.immutable.Stream$Cons
  println(s.isInstanceOf[scala.collection.immutable.Stream[_]]) // true
  println(s) // Stream(10,?)
  println(s.zipWithIndex) // Stream((10,0), ?)
  println(s(2)) // 30
  println(s) // Stream(10, 20, 30, ?)
  println(30 #:: s) // Stream(30, ?)
  println(s #::: s) // Stream(10, ?)
  println(s.toList) // List(10, 20, 30, 40, 50)

  def lazyNum[A](a: =>A) = {println("u"+a); a}
  val s2: Stream[Int] = lazyNum(10) #:: lazyNum(20) #:: lazyNum(30) #:: lazyNum(40) #:: Stream.empty
  println(s2) // "u10".. Stream(10,?)
  println(s2(2)) // "u20, u30".. 30
  println(s2) // Stream(10,20,30,?)
  val s3 = s2 #::: s2 // actually: 10,20,30,?,10,20,30,?
  println(s3) // Stream(10,?)
  println(s3(2)) // 30
  println(s3(5)) // "u40", 20
}
