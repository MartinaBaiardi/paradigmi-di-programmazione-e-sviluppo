package main.u05.collections.mytraversable

trait Traversable[A] {
  def foreach[U](f: A => U): Unit // the only undefined method
  def isEmpty: Boolean = { var v = true; foreach(a => v=false); v }
  def size: Int = { var v = 0; foreach(a => v=v+1); v }
  def forall(p: A => Boolean): Boolean = { var v = true; foreach(a => v = v & p(a)); v }
  def exists(p: A => Boolean): Boolean = { var v = false; foreach(a => v = v | p(a)); v }
  def find(p: A => Boolean): Option[A] = ???
  def foldLeft[B](z: B)(op: (B, A) => B): B = ???
  def foldRight[B](z: B)(op: (A, B) => B): B = ???
  def reduceLeft[B >: A](op: (B, A) => B): B = ???
  def reduceRight[B >: A](op: (A, B) => B): B = ???
  def head: A = ???
  def headOption: Option[A] = ???
  def tail: Traversable[A] = ???
  def last: A = ???
  def lastOption: Option[A] = ???
  def sortWith(lt : (A,A) => Boolean): Traversable[A] = ???
  def map[B](f: A => B): Traversable[B] = ???
  def flatMap[B](f: A => Traversable[B]): Traversable[B] = ???
  def filter(p: A => Boolean): Traversable[A] = ???
  def partition(p: A => Boolean): (Traversable[A], Traversable[A]) = ???
  def groupBy[K](f: A => K): Map[K, Traversable[A]] = ???
  def take(n: Int): Traversable[A] = ???
  def drop(n: Int): Traversable[A] = ???
  def slice(from: Int, until: Int): Traversable[A] = ???
  def splitAt(n: Int): (Traversable[A], Traversable[A]) = ???
  def toArray[B >: A]: Array[B] = ???
  def toList: List[A] = ???
  def toSeq: Seq[A] = ???
  def toStream: Stream[A] = ???
  def mkString(start: String, sep: String, end: String): String = ???
}

object Traversable extends App {
  val v = new Traversable[Int]{
    override def foreach[U](f: Int => U): Unit = (1 to 10).foreach(f)
  }
  println(v.size)
  println(v.isEmpty)

}
