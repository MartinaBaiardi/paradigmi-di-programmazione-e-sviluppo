package main.u05.collections

object UseParallel extends App {

  val SIZE = 100000

  // All collections have a par() method to turn them into parallel versions
  // Some of them gets swapped to other collections: beware overhead!

  val v = Vector.range(0,SIZE)
  println(v.getClass) // scala.collection.immutable.Vector
  val vp = v.par
  println(vp.getClass) // scala.collection.parallel.immutable.ParVector
  v.par.foreach (println(_)) // starts immediately, prints out of order

  val l = List.range(0,SIZE) //heavy operation!
  println(l.getClass) // scala.collection.immutable.$colon$colon
  val lp = l.par
  println(lp.getClass) // scala.collection.parallel.immutable.ParVector
  lp.foreach (println(_)) // starts immediately, prints out of order

  val s = Stream.iterate(0)(_+1).take(SIZE)
  println(s.getClass) // scala.collection.immutable.Stream$Cons
  val sp = s.par // heavy operation!
  println(sp.getClass) // scala.collection.parallel.immutable.ParVector
  sp.foreach (println(_)) // starts immediately, prints out of order

  def time[R](block: => R): R = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    println("Elapsed time: " + (t1 - t0) + "ns")
    result
  }

  val (v1,v2) = (Vector.range(0,SIZE),Vector.range(0,SIZE).par)
  println(time{v1.sum})
  println(time{v2.sum}) // faster with big SIZE

}
