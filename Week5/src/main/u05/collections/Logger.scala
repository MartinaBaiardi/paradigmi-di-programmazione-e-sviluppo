package main.u05.collections

// encapsulating the API
class Logger(private val consumer: (String)=>Unit) {

  private var strings = Seq[String]() // could use List

  def log(string: String): Unit = {strings = strings :+ string}

  def getLog: Traversable[String] = strings.toTraversable

  def emit(): Unit = strings.foreach(consumer)
}

// directly exposing the API.. a bad idea
class Logger2() extends scala.collection.mutable.MutableList[String]


object TryLogger extends App { // which one is better??
  val logger = new Logger( s => println("log: "+s) )
  logger.log("str1")
  logger.log("str2")
  logger.log("str3")
  logger.getLog foreach (println(_))
  logger.emit()

  //var logger2 = List[String]()
  val logger2 = new Logger2
  logger2 += "str1"
  logger2 += "str2"
  logger2 += "str3"
  logger2 foreach (println(_))
}
