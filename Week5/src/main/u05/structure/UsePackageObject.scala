package main.u05.structure

// Using myprint in another package requires just package import
object UsePackageObject extends App {

  import po._
  myprint()   // using a definition in po's package object
}
