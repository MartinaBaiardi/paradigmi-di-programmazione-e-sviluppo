package u05.pack  // the package of all definitions below

package a {   // entering a subpackage as a namespace
  object O1
  package b { // entering a further subpackage as a namespace
    object O2
    object A {  // I keep nesting stuff
      object O3
      object B {
        object O4
        class C{
          println(O1,O2,O3,O4,A,B,f()) // note visibility on top
          println(u05.pack.a.b.A.B.O4) // full qualification
        }
        def f():Unit = {
          println(O1,O2,O3,O4,A,B,new C)
        }
      }
    }
  }
}

package a {   // re-entering package u05.pack.a
  object O1B {
    val v = O1 // using u05.pack.a.O1
  }
}

object Packages extends App {
  println(u05.pack.a.b.A.B.O4)
  println(u05.pack.a.O1B)
}