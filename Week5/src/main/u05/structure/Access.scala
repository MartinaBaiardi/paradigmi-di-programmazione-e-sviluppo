package main.u05.structure

class C {
      private[this] object pri_this // visible only by this (object-private)
      private object pri            // visible only by C's instances
      private[pa2] object pri_pa2   // visible only up to pa2
      protected object pro          // visible to subclasses
      protected[pa2] object pro_pa2 // visible to subclasses, or up to pa2
      def f(c: C){
        //print(c.pri_this) // ALL ERRORS
        print(C.g())  // WORKS, can see companion's private stuff
      }
    }
    class C {
      private[this] object pri_this // visible only by this (object-private)
      private object pri            // visible only by C's instances
      private[pa2] object pri_pa2   // visible only up to pa2
      protected object pro          // visible to subclasses
      protected[pa2] object pro_pa2 // visible to subclasses, or up to pa2
      def f(c: C){
        //print(c.pri_this) // ALL ERRORS
        print(C.g())  // WORKS, can see companion's private stuff
      }
    }
    object C {
      // print(new C().pri_this) // ALL ERRORS
      print(new C().pri)         // WORKS, can see class' private stuff
      private def g() = 1
    }
    class D extends C {
      //print(pri_this,pri) // ALL ERRORS
    }
  class E(c: C) {
      //print(c.pri_this,c.pri,c.pro) // ALL ERRORS
      print(c.pri_pa2, c.pro_pa2) // WORKS!
    }


