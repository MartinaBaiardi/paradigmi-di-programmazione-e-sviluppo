package main.u05.structure

// Same example as previous one, but with structural types
object StructuralTypes extends App {

  // comp is any object that has the compare method
  def min(list: List[Int],
          comp: {def compare(a: Int, b: Int): Int}): Option[Int] = {
    @annotation.tailrec
    def min2(list:List[Int], minSoFar: Int): Int = list match {
      case h::t if (comp.compare(h,minSoFar)>0) => min2(t, minSoFar)
      case h::t => min2(t, h)
      case _ => minSoFar
    }
    //if (list.isEmpty) None else Some(min2(list.tail, list.head))
    Some(list) filter (! _.isEmpty) map (l => min2(l.tail,l.head))
  }

    // Instantiating an anonymous class with just the comp method
  val comp = new {
    def compare(a: Int, b: Int): Int = a-b
  }

  // The compiler correctly types the call, but it's expensive at run-time
  println(min(List(10,20,5,40),comp)) // Some(5)
  println(min(List(),comp)) // None
}
