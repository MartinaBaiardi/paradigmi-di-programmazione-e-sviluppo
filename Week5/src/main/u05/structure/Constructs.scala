package main.u05.structure

object Constructs extends App {

  // nestable constructs
  class C
  abstract class D
  object O
  trait T
  def f():Unit = {}

  println(new C, new D{}, O, new T{}, f())

  // nestable constructs with some content
  class C2 { val a = 1 }
  abstract class D2 { val b = 2 }
  object O2 { val c = 3 }
  trait T2 { val d = 4 }
  def f2():Unit = { print("hello")}
  def g2(a: {val e: Int}): Int = a.e // arg 'a' has structural type

  println(new C2().a, new D2{}.b, O2.c, new T2{}.d, f2())
  println(g2(new {val e = 5}))
  // we are passing to g2 a compatible object

}
