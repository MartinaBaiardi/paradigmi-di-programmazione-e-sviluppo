package main.u05.lists.slist

// List as a mostly-pure interface
sealed trait List[A] {

  def head: Option[A]

  def tail: Option[List[A]]

  def append(list: List[A]): List[A]

  def foreach(consumer: A=>Unit): Unit

  def get(pos: Int): Option[A]

  def filter(predicate: A=>Boolean): List[A]

  def map[B](fun: A=>B): List[B]

  def prepend(head: A): List[A]

  // right-associative construction: 10 :: 20 :: 30 :: Nil()
  def ::(head: A): List[A] = prepend(head)

  def isEmpty: Boolean

}

// defining concrete implementations based on the same template

case class Cons[A](h: A, t: List[A])
  extends ListImplementation[A]

case class Nil[A]()
  extends ListImplementation[A]

// additionally enabling pattern matching on ::

object :: {
  def unapply[A](l: List[A]): Option[(A,List[A])] = l match {
    case Cons(h,t) => Some((h,t))
    case _ => None
  }
}

trait ListImplementation[A] extends List[A] {
  override def head: Option[A] = this match {
    case h :: t => Some(h)
    case _ => None
  }
  override def tail: Option[List[A]] = this match {
    case h :: t => Some(t)
    case _ => None
  }
  override def append(list: List[A]): List[A] = this match {
    case h :: t => h :: (t append list)
    case _ => list
  }
  override def foreach(consumer: (A)=>Unit): Unit = this match {
    case h :: t => {consumer(h); t foreach consumer}
    case _ => None
  }
  override def get(pos: Int): Option[A] = this match {
    case h :: t if pos == 0 => Some(h)
    case h :: t if pos > 0 => t get (pos-1)
    case _ => None
  }
  override def filter(predicate: (A) => Boolean): List[A] = this match {
    case h :: t if (predicate(h)) => h :: (t filter predicate)
    case _ :: t => (t filter predicate)
    case _ => Nil()
  }
  override def map[B](fun: (A) => B): List[B] = this match {
    case h :: t => fun(h) :: (t map fun)
    case _ => Nil()
  }
  override def prepend(head: A): List[A] = Cons(head,this)

  override def isEmpty: Boolean = this.isInstanceOf[Nil[_]]

}


object List {

  def apply[A](elems: A*): List[A] = {
    var list: List[A] = Nil()
    for (e <- elems.reverse) list = e :: list
    list
  }

  def of[A](elem: A, n: Int): List[A] =
    if (n==0) Nil() else elem :: of(elem,n-1)
}

object Test extends App {

  import List._ // Working with the above lists
  println(List(10, 20, 30, 40))
  val l = 10 :: 20 :: 30 :: 40 :: Nil() // same as above
  println(l.head) // 10
  println(l.tail) // 20,30,40
  println(l append l) // 10,20,30,40,10,20,30,40
  println(l get 2) // 30
  println(of("a", 10)) // a,a,a,..,a
  println(l filter (_ <= 20) map ("a" + _)) // a10, a20
  println(for (i <- l) yield "a" + i) //a10,a20,a30,a40

}
