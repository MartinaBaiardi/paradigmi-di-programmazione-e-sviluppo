package main.u05.lists.oolist

// List as a mostly-pure interface
sealed trait List[A] {

  def head: Option[A]

  def tail: Option[List[A]]

  def append(list: List[A]): List[A]

  def foreach(consumer: A=>Unit): Unit

  def get(pos: Int): Option[A]

  def filter(predicate: A=>Boolean): List[A]

  def map[B](fun: A=>B): List[B]

  def prepend(head: A): List[A]

  // right-associative construction: 10 :: 20 :: 30 :: Nil()
  def ::(head: A): List[A] = prepend(head)
}

// Cons as one of the possible implementations
case class Cons[A](h: A, t: List[A]) extends List[A] {

  override def head: Option[A] = Some(h)

  override def tail: Option[List[A]] = Some(t)

  override def append(list: List[A]): List[A] = h :: (t append list)

  override def foreach(consumer: (A) => Unit): Unit = {
    consumer(h)
    t foreach consumer
  }

  override def get(pos: Int): Option[A] =
    if (pos==0) Some(h) else t get (pos-1)

  override def filter(predicate: (A) => Boolean): List[A] = {
    val v = t filter predicate
    if (predicate(h)) h :: v else v
  }

  override def map[B](fun: (A) => B): List[B] =
    fun(h) :: (t map fun)

  override def prepend(head: A): List[A] = Cons(head,this)
}

// Nil as another implementation
case class Nil[A]() extends List[A] {

  override def head: Option[A] = None

  override def tail: Option[List[A]] = None

  override def append(list: List[A]): List[A] = list

  override def foreach(consumer: (A) => Unit): Unit = {}

  override def get(pos: Int): Option[A] = None

  override def filter(predicate: (A) => Boolean): List[A] = Nil()

  override def map[B](fun: (A) => B): List[B] = Nil()

  override def prepend(head: A): List[A] = Cons(head,Nil())
}

// A module with apply and other functionalities
object List {

  def apply[A](elems: A*): List[A] = {
    var list: List[A] = Nil()
    for (e <- elems.reverse) list = e :: list
    list
  }

  def of[A](elem: A, n: Int): List[A] =
    if (n==0) Nil() else elem :: of(elem,n-1)
}

object Test extends App {

  import List._  // Working with the above lists
  println(List(10,20,30,40))
  val l = 10 :: 20 :: 30 :: 40 :: Nil() // same as above
  println(l.head) // 10
  println(l.tail) // 20,30,40
  println(l append l) // 10,20,30,40,10,20,30,40
  println(l get 2) // 30
  println(of("a",10)) // a,a,a,..,a
  println(l filter (_<=20) map ("a"+_) ) // a10, a20
  println(l.filter(_<=20).map("a"+_) ) // a10, a20
  println(for (i <- l) yield "a"+i) //a10,a20,a30,a40
  for (i <- List(10,20,30)) {print(" "+i)}
}