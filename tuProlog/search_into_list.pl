% Funzione di ricerca di un elemento all'interno di una lista: search(L,E), if E è un elemento della lista
% In prolog ci dobbiamo preoccupare solamente dei casi positivi.


% Se l'elemento c'è subito (in testa)
search( cons(E, _), E).

% Se l'elemento è cercato ricorsivamente
search( cons(H, T), E) :- search(T, E). % Applico la sostituzione theta ottenuta dalla head della clausola nel body

% [H|T] == cons(H, T)
% [] == nil
% [H1,H2] == cons(H1, cons(H2, nil))
% [H1, H2|T] == cons(H1, cons(H2, T))