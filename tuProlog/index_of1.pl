% IndexOf E

index([E|_], E, 0).

index([_|T], E, N) :- index(T, E, M), N is M+1.

% GOAL: index([10,20,30,40],30, X).
%	index([10,20,30,40],N, X).