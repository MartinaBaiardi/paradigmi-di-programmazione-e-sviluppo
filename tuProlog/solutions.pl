% search_two(List,Elem)
% looks for two occurrences of Elem with any element in between!

search_two( [ E, _, E | _ ], E).
search_two( [ _ | T ], E) :- search_two(T, E).

% size(List,Size)
% Size will contain the number of elements in List, written using notation zero, s(zero), s(s(zero))..

size([], zero).
size([_|T], s(R)) :- size(T, R).

% sum(List,Sum)

sum([], 0).
sum([H|T], N) :- sum(T, R), N is R + H.

% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element.

max([H | T], M) :- max(T, M, H).
max([], M, M).
max([H | T], M, Temp) :- H > Temp, max(T,M,H).
max([H | T], M, Temp) :- H < Temp, max(T,M,Temp).

% max(List,Max,Min)
% Max is the biggest element in List
% Min is the smallest element in List
% Suppose the list has at least one element

min_max( [H|T], Max, Min ) :- min_max( T, Max, H, Min, H).
min_max( [], Max, Max, Min, Min).
min_max( [H|T], Max, Temp1, Min, Temp2) :- H > Temp1, min_max( T, Max, H, Min, Temp2).
min_max( [H|T], Max, Temp1, Min, Temp2) :- H < Temp2, min_max( T, Max, Temp1, Min, H). 

% all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1
% example: all_bigger([10,20,30,40],[9,19,29,39]).

all_bigger([],[]).
all_bigger([X|Xs],[Y|Ys]):- X > Y, all_bigger(Xs,Ys).

% sublist(List1,List2)
% List1 should contain elements all also in List2
% example: sublist([1,2],[5,3,2,1]).

search( [ E | _ ], E).
search( [ _ | T ], E) :- search(T, E).

sublist([],_).
sublist([H|T], List) :- search(List, H), sublist(T, List).

% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).

seq(0, _, []).
seq(M, N, [N|T]):- M > 0, N2 is N-1, M2 is M-1, seq(M2, N2, T).
seqR(N, List) :- N2 is N+1, seq(N2, N, List).

% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).

seq(M, M, []).
seq(M, N, [N|T]):- N2 is N+1, seq(M, N2, T).
seqR2(N, List) :- N2 is N+1, seq(N2, 0, List).

% inv(List,List)
% example: inv([1,2,3],[3,2,1]).

inv([], L, L).
inv([H|T], L, R) :- inv(T, L, [H|R]).
inv(L1, L2) :- inv(L1, L2, []).

% double(List,List)
% suggestion: remember predicate append/3
% example: double([1,2,3],[1,2,3,1,2,3]).

append([], L, L).
append([H|T], L, [H|R]) :- append(T, L, R).
double(L1, L2) :- append(L1, L1, L2).

% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).

times(L1, 0, []).
times(L1, 0, L2, L2).
times(L1, N, L2, R) :- N>0, N2 is N-1, times(L1, N2, L2, R2), append(L1, R, R2).
times(L1, N, L2) :- N>0, times(L1, N, L2, []). 

% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).

proj([], []).
proj([[H|_]|T], [H|R]) :- proj(T, R).

