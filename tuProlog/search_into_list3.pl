
% Se l'elemento c'è subito (in testa)
search( [ E, E | _ ], E).

% Se l'elemento è cercato ricorsivamente
search( [ _ | T ], E) :- search(T, E). % Applico la sostituzione theta ottenuta dalla head della clausola nel body

% GOAL: search([a,b], b).
%	search(L, b).