% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element

max([H | T], M) :- max(T, M, H).
max([], M, M).
max([H | T], M, Temp) :- H > Temp, max(T,M,H).
max([H | T], M, Temp) :- H < Temp, max(T,M,Temp).
