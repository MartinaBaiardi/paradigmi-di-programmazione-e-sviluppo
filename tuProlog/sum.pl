% sum(List,Sum)

sum([], 0).
sum([H|T], N) :- sum(T, R), N is R + H.