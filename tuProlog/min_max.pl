% max(List,Max,Min)
% Max is the biggest element in List
% Min is the smallest element in List
% Suppose the list has at least one element

min_max( [H|T], Max, Min ) :- min_max( T, Max, H, Min, H).
min_max( [], Max, Max, Min, Min).
min_max( [H|T], Max, Temp1, Min, Temp2) :- H > Temp1, min_max( T, Max, H, Min, Temp2).
min_max( [H|T], Max, Temp1, Min, Temp2) :- H < Temp2, min_max( T, Max, Temp1, Min, H). 
