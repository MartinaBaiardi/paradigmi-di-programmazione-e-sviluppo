% IndexOf E

index([E|_], E, zero).

index([_|T], E, s(R)) :- index(T, E, R).

% GOAL: index([10,20,30,40],30, X).