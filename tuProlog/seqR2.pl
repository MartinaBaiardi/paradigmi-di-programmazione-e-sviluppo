% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).

seq(M, M, []).
seq(M, N, [N|T]):- N2 is N+1, seq(M, N2, T).

seqR2(N, List) :- N2 is N+1, seq(N2, 0, List).