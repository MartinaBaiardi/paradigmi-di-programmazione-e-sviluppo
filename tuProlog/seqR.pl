% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).


seq(0, _, []).
seq(M, N, [N|T]):- M > 0, N2 is N-1, M2 is M-1, seq(M2, N2, T).

seqR(N, List) :- N2 is N+1, seq(N2, N, List).
