% size(List, Size)
% Size will contain the number of elements in List

size([], 0).
size([_|T], N) :- size(T, M), N is M+1.

%Note: Built-in numbers are extra-relational!!