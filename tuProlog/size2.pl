% size(List,Size)
% Size will contain the number of elements in List, written using notation zero, s(zero), s(s(zero))..

size([], zero).
size([_|T], s(R)) :- size(T, R).