% Numeri naturali:
% zero, s(zero), s(s(zero)), s(s(s(zero))), ...

sum (zero, N, N). 			% Caso base
sum (s(N), M, s(R)) :- sum(N, M, R).	% Questo caso comprende tutti i numeri successivi allo zero. R := risultato parziale.