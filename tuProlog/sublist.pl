% sublist(List1,List2)
% List1 should contain elements all also in List2
% example: sublist([1,2],[5,3,2,1]).

search( [ E | _ ], E).
search( [ _ | T ], E) :- search(T, E).

sublist([],_).
sublist([H|T], List) :- search(List, H), sublist(T, List).