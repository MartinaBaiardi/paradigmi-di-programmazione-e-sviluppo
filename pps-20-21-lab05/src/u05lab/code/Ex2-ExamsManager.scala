package u05lab.code

import u05lab.code
import u05lab.code.ExamResult.Kind
import u05lab.code.ExamResult.Kind.Succeeded

import java.lang.StackWalker
import scala.::
import scala.Option.option2Iterable
import scala.collection.immutable.{Map, _}

sealed trait ExamResult{
  def getKind: Kind
  def getEvaluation: Option[Int]
  def cumLaude: Boolean
}
object ExamResult{
  def apply(k: Kind): ExamResult = ExamResultImpl(k)
  def failed: ExamResult = ExamResultImpl(Kind.Failed())
  def retired: ExamResult = ExamResultImpl(Kind.Retired())
  def succeeded(vote: Int): ExamResult = vote match {
    case v if (v < 18 || v > 30) => throw new IllegalArgumentException
    case _ => ExamResultImpl(Kind.Succeeded(vote, false))
  }
  def succeededCumLaude: ExamResult = ExamResultImpl(Kind.Succeeded(30, true))

  private case class ExamResultImpl(k: Kind) extends ExamResult{
    override def getKind: Kind = k

    override def getEvaluation: Option[Int] = k match {
      case Kind.Succeeded(v, _) => Some(v)
      case _ => Option.empty
    }

    override def cumLaude: Boolean = k match {
      case Kind.Succeeded(_, l) => l
      case _ => false
    }

    override def toString: String = k match {
      case Kind.Succeeded(v, l) if l => k.toString+"("+v+"L)"
      case Kind.Succeeded(v, _) => k.toString+"("+v+")"
      case _ => k.toString
    }
  }

  trait Kind
  object Kind {
    case class Failed() extends Kind {
      override def toString: String = "FAILED"
    }
    case class Retired() extends Kind{
      override def toString: String = "RETIRED"
    }
    case class Succeeded(vote: Int, laude: Boolean) extends Kind {
      override def toString: String = "SUCCEEDED"
    }
  }
}

sealed trait ExamsManager{
  def createNewCall(month: String): Unit
  def addStudentResult(call: String, student: String, result: ExamResult): Unit
  def getAllStudentsFromCall(call: String): Set[String]
  def getEvaluationsMapFromCall(call: String): Map[String, Int]
  def getResultsMapFromStudent(student: String): Map[String, ExamResult]
  def getBestResultFromStudent(student: String): Option[Int]
}
object ExamsManager{
  def apply(): ExamsManager = ExamsManagerImpl()

  private case class ExamsManagerImpl() extends ExamsManager{
    private var manager = Map[String, Map[String, ExamResult]]()

    override def createNewCall(month: String): Unit = {
      if (manager.contains(month)) throw new IllegalArgumentException
      manager = manager + (month -> Map())
    }

    override def addStudentResult(call: String, student: String, result: ExamResult): Unit = {
      val m = manager.getOrElse(call, throw new IllegalArgumentException)
      if (m.contains(student)) throw new IllegalArgumentException
      // manager = manager.updated(call, m + (student -> result))
      manager = manager + (call -> (m + (student -> result)))
    }

    override def getAllStudentsFromCall(call: String): Set[String] = manager(call).keySet

    override def getEvaluationsMapFromCall(call: String): Map[String, Int] = {
      val m = manager.getOrElse(call, throw new IllegalArgumentException)
      m.collect({case (student, result) if result.getEvaluation.nonEmpty => student -> result.getEvaluation.get})
    }

    override def getResultsMapFromStudent(student: String): Map[String, ExamResult] =
      manager.collect({case (call, results) if results.contains(student) => call -> results(student) })

    override def getBestResultFromStudent(student: String): Option[Int] =
    {
      val r = getResultsMapFromStudent(student).collect({case (_, result) if result.getEvaluation.nonEmpty => result.getEvaluation.get})
      if (r.isEmpty) Option.empty else Some(r.max)

      //manager.values.flatMap(m => m.filterKeys(s => s == student)).map{case (s, r) => r.getEvaluation}.max
    }
  }
}
