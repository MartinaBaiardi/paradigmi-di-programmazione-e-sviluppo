package u05lab


object Ex4_Sequence extends App{

  def sequence[A](a: List[Option[A]]): Option[List[A]] = a.contains(None) match {
    case true => Option.empty
    case false => Some(a.map(_.get))
  }


  println(sequence(List(Some(1), Some(2), Some(3))))
  println(sequence(List(Some(1), None, Some(3))))

  // foldLeft preserves list order

}
