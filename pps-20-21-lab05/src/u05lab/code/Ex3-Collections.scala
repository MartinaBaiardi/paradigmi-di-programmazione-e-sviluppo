package u05lab

import u05lab.PerformanceUtils.measure

import java.util.concurrent.TimeUnit
import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.collection.parallel.immutable._
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  println()

  /* Linear sequences: List, ListBuffer */
  measure("List[Int]               ") {
    var list: List[Int] = List(20, 30, 40, 50)
    val x = list(3)
    val y = list.size
    list = 10 :: list //Prepend
    list = list :+ 60 //Append
    list = list.diff(List(40)) //Remove
  }

  measure("ListBuffer[Int]         ") {
    val buffer: ListBuffer[Int] = ListBuffer(20, 30, 40, 50)
    val x = buffer(3)
    val y = buffer.size
    10 +=: buffer //Prepend
    buffer += 60 //Append
    buffer -= 40 //Remove
  }

  /* Indexed sequences: Vector, Array, ArrayBuffer */
  measure("Vector[Int]             ") {
    var vector: Vector[Int] = Vector(20, 30, 40, 50)
    val x = vector(3)
    val y = vector.size
    vector = 10 +: vector //Prepend
    vector = vector :+ 60 //Append
    vector = vector.filter(_ != 40) //Remove
  }

  measure("Array[Int]              ") {
    var array: Array[Int] = Array(20, 30, 40, 50) //Array is from Java arrays: in output we have an object
    val x = array(3)
    val y = array.length
    array = 10 +: array //Prepend: We can't modify Array's size, in order to do this we need to create a new Array
    array = array :+ 60 //Append:  We can't modify Array's size, in order to do this we need to create a new Array
    array(3) = 0
  }

  measure("ArrayBuffer[Int]        ") {
    val arrayBuffer: ArrayBuffer[Int] = ArrayBuffer(20, 30, 40, 50)
    val x = arrayBuffer(3)
    val y = arrayBuffer.size
    10 +=: arrayBuffer //Prepend
    arrayBuffer += 60 //Append
    arrayBuffer -= 40 //Remove
  }

  /* Sets */
  measure("Set[Int]                ") {
    var immutableSet: Set[Int] = Set(20, 30, 40, 50)
    val x = immutableSet(3)
    val y = immutableSet.size
    immutableSet += 60 //Insert
    immutableSet -= 40 //Remove
  }

  measure("mutable.Set[Int]        ") {
    val mutableSet: mutable.Set[Int] = mutable.Set(20, 30, 40, 50)
    val x = mutableSet(3)
    val y = mutableSet.size
    mutableSet += 60 //Insert
    mutableSet -= 40 //Remove
  }

  /* Maps */
  measure("Map[Int, String]        ") {
    var immutableMap: Map[Int, String] = Map(20 -> "a", 30 -> "b", 40 -> "c", 50 -> "d")
    val x = immutableMap(30)
    val y = immutableMap.size
    immutableMap += (60 -> "e") //Insert
    immutableMap -= 40 //Remove
  }

  measure("mutable.Map[Int, String]"){
    val mutableMap: mutable.Map[Int, String] = mutable.Map(20-> "a", 30 -> "b", 40 -> "c", 50 -> "d")
    val x = mutableMap(30)
    val y = mutableMap.size
    mutableMap += (60 -> "e")         //Insert
    mutableMap -= 40                  //Remove
  }

  println()

  /* Comparison */
  import PerformanceUtils._
  val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector
  assert( measure("lst last"){ lst.last } > measure("vec last"){ vec.last } )
}