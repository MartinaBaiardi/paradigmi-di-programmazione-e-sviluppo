package u05lab

import org.junit.jupiter.api.Assertions.{assertEquals, assertFalse, assertThrows, assertTrue, fail}
import org.junit.jupiter.api.Test
import u05lab.code.{ExamResult, ExamsManager}

import scala.collection.immutable.HashSet

class Ex2Tests {

  @Test
  def testFailed(): Unit ={
    assertTrue(ExamResult.failed.getKind.isInstanceOf[ExamResult.Kind.Failed])
    assertEquals(None, ExamResult.failed.getEvaluation)
    assertFalse(ExamResult.failed.cumLaude)
    assertEquals("FAILED", ExamResult.failed.toString)
  }

  @Test
  def testRetired(): Unit ={
    assertTrue(ExamResult.retired.getKind.isInstanceOf[ExamResult.Kind.Retired])
    assertEquals(None, ExamResult.retired.getEvaluation)
    assertFalse(ExamResult.retired.cumLaude)
    assertEquals("RETIRED", ExamResult.retired.toString)
  }

  @Test
  def testLaude(): Unit = {
    assertTrue(ExamResult.succeededCumLaude.getKind.isInstanceOf[ExamResult.Kind.Succeeded])
    assertEquals(Some(30), ExamResult.succeededCumLaude.getEvaluation)
    assertTrue(ExamResult.succeededCumLaude.cumLaude)
    assertEquals("SUCCEEDED(30L)", ExamResult.succeededCumLaude.toString)
  }

  @Test
  def testSucceeded(): Unit = {
    assertTrue(ExamResult.succeeded(28).getKind.isInstanceOf[ExamResult.Kind.Succeeded])
    assertEquals(Some(28), ExamResult.succeeded(28).getEvaluation)
    assertFalse(ExamResult.succeeded(28).cumLaude)
    assertEquals("SUCCEEDED(28)", ExamResult.succeeded(28).toString)
  }

  @Test
  def testHigherVote(): Unit ={
    try {
      ExamResult.succeeded(32)
    } catch {
      case _:IllegalArgumentException => return
    }
    fail()
  }

  @Test
  def testLowerVote(): Unit ={
    try {
      ExamResult.succeeded(17)
    } catch {
      case _:IllegalArgumentException => return
    }
    fail()
  }

  val em:ExamsManager = ExamsManager()

  private def prepareExams() {
    em.createNewCall("gennaio");
    em.createNewCall("febbraio");
    em.createNewCall("marzo");

    em.addStudentResult("gennaio", "rossi", ExamResult.failed) // rossi -> fallito
    em.addStudentResult("gennaio", "bianchi", ExamResult.retired); // bianchi -> ritirato
    em.addStudentResult("gennaio", "verdi", ExamResult.succeeded(28)); // verdi -> 28
    em.addStudentResult("gennaio", "neri", ExamResult.succeededCumLaude); // neri -> 30L

    em.addStudentResult("febbraio", "rossi", ExamResult.failed); // etc..
    em.addStudentResult("febbraio", "bianchi", ExamResult.succeeded(20));
    em.addStudentResult("febbraio", "verdi", ExamResult.succeeded(30));

    em.addStudentResult("marzo", "rossi", ExamResult.succeeded(25));
    em.addStudentResult("marzo", "bianchi", ExamResult.succeeded(25));
    em.addStudentResult("marzo", "viola", ExamResult.failed);
  }

  @Test
  def testGetAllStudentsFromCall() {
    this.prepareExams();

    // partecipanti agli appelli di gennaio e marzo
    assertEquals(Set("rossi", "bianchi", "verdi", "neri"), em.getAllStudentsFromCall("gennaio"))
    assertEquals(Set("rossi", "bianchi", "viola"), em.getAllStudentsFromCall("marzo"))
  }

  @Test
  def testGetEvaluationsMapFromCall {
    this.prepareExams();

    // promossi di gennaio con voto
    assertEquals(2, em.getEvaluationsMapFromCall("gennaio").size);
    assertEquals(28, em.getEvaluationsMapFromCall("gennaio")("verdi"))
    assertEquals(30, em.getEvaluationsMapFromCall("gennaio")("neri"))

    // promossi di febbraio con voto
    assertEquals(2, em.getEvaluationsMapFromCall("febbraio").size)
    assertEquals(20, em.getEvaluationsMapFromCall("febbraio")("bianchi"));
    assertEquals(30, em.getEvaluationsMapFromCall("febbraio")("verdi"));

  }


  @Test
  def testGetResultsMapFromCall {
    this.prepareExams();

    // tutti i risultati di rossi (attenzione ai toString!!)
    assertEquals(3, em.getResultsMapFromStudent("rossi").size)
    assertEquals(ExamResult.failed, em.getResultsMapFromStudent("rossi")("gennaio"))
    assertEquals(ExamResult.failed, em.getResultsMapFromStudent("rossi")("febbraio"))
    assertEquals(ExamResult.succeeded(25), em.getResultsMapFromStudent("rossi")("marzo"))

    // tutti i risultati di bianchi
    assertEquals(3, em.getResultsMapFromStudent("bianchi").size)
    assertEquals(ExamResult.retired, em.getResultsMapFromStudent("bianchi")("gennaio"))
    assertEquals(ExamResult.succeeded(20), em.getResultsMapFromStudent("bianchi")("febbraio"))
    assertEquals(ExamResult.succeeded(25), em.getResultsMapFromStudent("bianchi")("marzo"))

    // tutti i risultati di neri
    assertEquals(1, em.getResultsMapFromStudent("neri").size);
    assertEquals(ExamResult.succeededCumLaude, em.getResultsMapFromStudent("neri")("gennaio"))
  }

  @Test
  def optionalTestExamsManagement() {
    this.prepareExams();
    // miglior voto acquisito da ogni studente, o vuoto..
    assertEquals(Some(25), em.getBestResultFromStudent("rossi"))
    assertEquals(Some(25), em.getBestResultFromStudent("bianchi"))
    assertEquals(Some(30), em.getBestResultFromStudent("neri"))
    assertEquals(Option.empty, em.getBestResultFromStudent("viola"));
  }


  @Test
  def optionalTestCantCreateACallTwice() {
    this.prepareExams();
    try{
      em.createNewCall("marzo");
      fail()
    } catch {
      case _:IllegalArgumentException =>
    }
  }

  @Test
  def optionalTestCantRegisterAnEvaluationTwice() {
    this.prepareExams();
    try{
      em.addStudentResult("gennaio", "verdi", ExamResult.failed)
      fail()
    } catch {
      case _:IllegalArgumentException =>
    }
  }
}
