package u05lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class Ex1Tests {

  val l1 = List("a", "b", "c")
  val l2 = List(10, 20, 30, 40)

  @Test
  def testZipRight() {

    assertEquals(List.nil, List.nil.zipRight)
    assertEquals(List(("a", 0), ("b", 1), ("c", 2)), l1.zipRight)
  }

  @Test
  def testPartition(): Unit ={
    assertEquals((List(10,20), List(30, 40)), l2.partition(_ < 25))
    assertEquals((List("b"), List("a", "c")), l1.partition( _ == "b"))
  }

  @Test
  def testSpan(): Unit = {
    assertEquals(( Nil(), Cons(10,Cons(20,Cons(30,Cons(40,Nil())))) ), l2.span(_>15))
    assertEquals(( Cons(10,Nil()), Cons(20,Cons(30,Cons(40,Nil()))) ), l2.span(_<15))
  }

  @Test
  def testReduce(): Unit = {
    assertEquals(100,l2.reduce(_+_))
    try {
      List[Int]().reduce(_+_);
      fail()
    } catch {
      case _:UnsupportedOperationException =>
    }
  }

  @Test
  def testTakeRight(): Unit ={
    assertEquals(Cons(30,Cons(40,Nil())), l2.takeRight(2))
    assertEquals(4, l2.size())

    val l = List("a", "b", "c", "d", "e")
    assertEquals(5, l.size())
    assertEquals(List("c", "d", "e"), l.takeRight(3))

  }

  @Test
  def testCollect(): Unit ={
    assertEquals(Cons(9, Cons(39, Nil())), l2.collect { case x if x<15 || x>35 => x-1 })
    assertEquals(Cons("Testb", Cons("Testc", Nil())), l1.collect { case x if x == "b" || x == "c" => "Test"+x })
  }
}