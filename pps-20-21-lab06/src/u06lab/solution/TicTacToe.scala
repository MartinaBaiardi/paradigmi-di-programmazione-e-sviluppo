package u06lab.solution

import scala.annotation.tailrec
import scala.util.Random

object TicTacToe extends App{
  sealed trait Player{
    def other: Player = this match {case X => O; case _ => X}
    override def toString: String = this match {case X => "X"; case _ => "O"}
  }
  case object X extends Player
  case object O extends Player

  case class Mark(x: Int, y: Int, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Int, y: Int): Option[Player] = board.find(m => m.x == x && m.y == y).map(_.player)

    /*board collect {case Mark(`x`, `y`, player) => player} match {
    case h :: _ => Some(h)
    case _ => Option.empty
    }*/

  def placeAnyMark(board: Board, player: Player): Seq[Board] =
    for {x <- 0 to 2; y <- 0 to 2; if find(board, x, y) isEmpty} yield Mark(x, y, player) :: board


  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(Nil))
    case _ => for {
      game <- computeAnyGame(player.other, moves-1);
      board <- placeAnyMark(game.head, player)
      if !someoneWon(board)
    } yield board :: game
  }

  def someoneWon(board: Board): Boolean = {
    var won = false
    for (player <- List(X, O)){
      for (pos <- 0 to 2){
        if (List(Mark(pos, 0, player), Mark(pos, 1, player), Mark(pos, 2, player)).forall(board.contains) ||
          List(Mark(0, pos, player), Mark(1, pos, player), Mark(2, pos, player)).forall(board.contains)) won = true
      }
      if (List(Mark(0, 0, player), Mark(1, 1, player), Mark(2, 2, player)).forall(board.contains) ||
        List(Mark(0, 2, player), Mark(1, 1, player), Mark(2, 0, player)).forall(board.contains)) won = true
    }
    won
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  // Exercise 1: implement find such that..
  println(find(List(Mark(0,0,X)),0,0)) // Some(X)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),0,1)) // Some(O)
  println(find(List(Mark(0,0,X),Mark(0,1,O),Mark(0,2,X)),1,1)) // None

  // Exercise 2: implement placeAnyMark such that..
  printBoards(placeAnyMark(List(),X))
  //... ... ..X ... ... .X. ... ... X..
  //... ..X ... ... .X. ... ... X.. ...
  //..X ... ... .X. ... ... X.. ... ...
  println()
  printBoards(placeAnyMark(List(Mark(0,0,O)),X))
  //O.. O.. O.X O.. O.. OX. O.. O..
  //... ..X ... ... .X. ... ... X..
  //..X ... ... .X. ... ... X.. ...
  println()
  printBoards(placeAnyMark(List(Mark(0,0,O), Mark(2,2,O)),X))
  //O.. O.X O.. O.. OX. O.. O..
  //..X ... ... .X. ... ... X..
  //..O ..O .XO ..O ..O X.O ..O
  println()

  // Exercise 3 (ADVANCED!): implement computeAnyGame such that..
  var s = computeAnyGame(O, 4)
  s foreach {g => printBoards(g); println()}
  println(s.size)
  //... X.. X.. X.. XO.
  //... ... O.. O.. O..
  //... ... ... X.. X..
  //              ... computes many such games (they should be 9*8*7*6 ~ 3000).. also, e.g.:
  //
  //... ... .O. XO. XOO
  //... ... ... ... ...
  //... .X. .X. .X. .X.

  // Exercise 4 (VERY ADVANCED!) -- modify the above one so as to stop each game when someone won!!
}
