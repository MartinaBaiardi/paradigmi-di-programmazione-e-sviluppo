package u06lab.solution

import u06lab.solution.Combiner.{ConcatCombiner, MaxCombiner, SumCombiner}


trait Combiner[A] {
  def unit: A
  def combine(a: A, b: A): A
}
object Combiner {

  implicit object SumCombiner extends Combiner[Double] {
    override def unit: Double = 0
    override def combine(a: Double, b: Double): Double = a + b
  }

  implicit object ConcatCombiner extends Combiner[String] {
    override def unit: String = ""
    override def combine(a: String, b: String): String = a + b
  }

  implicit object MaxCombiner extends Combiner[Int] {
    override def unit: Int = Int.MinValue
    override def combine(a: Int, b: Int): Int = if (a > b) a else b
  }
}

trait Functions {
  def sum(a: List[Double]): Double
  def concat(a: Seq[String]): String
  def max(a: List[Int]): Int // gives Int.MinValue if a is empty
}
object FunctionsImpl extends Functions {

  override def sum(a: List[Double]): Double = {
    combine(a)
  }

  override def concat(a: Seq[String]): String = {
    combine(a)
  }

  override def max(a: List[Int]): Int = {
    combine(a)
  }

  def combine[A](a: Iterable[A])(implicit combiner: Combiner[A]): A = {
    a.foldLeft(combiner.unit)(combiner.combine)
  }
}

object TryFunctionsComb extends App {
  val f: Functions = FunctionsImpl
  println(f.sum(List(10.0,20.0,30.1))) // 60.1
  println(f.sum(List()))                // 0.0
  println(f.concat(Seq("a","b","c")))   // abc
  println(f.concat(Seq()))              // ""
  println(f.max(List(-10,3,-5,0)))      // 3
  println(f.max(List()))                // -2147483648
}