package u04lab.code

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ComplexTest {

  def complex1: Complex = Complex(5,4)
  def complex2: Complex = Complex(5,4)

  @Test def testComplex(): Unit ={
    println("complex1: " + complex1 + " and complex2: " + complex2)
//  Without case class
//  complex1 u04lab.code.Complex$ComplexImpl@7e0b85f9 and complex2: u04lab.code.Complex$ComplexImpl@63355449

//  With case class
//  complex1: ComplexImpl(5.0,4.0) and complex2: ComplexImpl(5.0,4.0)


    assertEquals(complex1, complex2)
//  Without case class
//  Expected :u04lab.code.Complex$ComplexImpl@470f1802
//  Actual   :u04lab.code.Complex$ComplexImpl@63021689

//  With case class they are equals
  }

}
