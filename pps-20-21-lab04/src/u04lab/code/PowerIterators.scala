package u04lab.code

import Optionals._
import u04lab.code.StreamExtension.listToStream
import u04lab.code.Streams._

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIterator(Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIterator(listToStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIterator(Stream.take(Stream.generate(Random.nextBoolean()))(size))
}

object PowerIterator {
  def apply[A](stream: Stream[A]): PowerIterator[A] = PowerIteratorImpl(stream)

  private case class PowerIteratorImpl[A](private var _stream: Stream[A]) extends PowerIterator[A] {
    private var _pastList: List[A] = List.Nil()

    override def next(): Option[A] = _stream match {
      case List.Cons(h, t) => {
        _stream = t()
        _pastList = List.Cons(h,  _pastList);
        Option.of(h())
      }
    }

    override def allSoFar(): List[A] = List.reverse(_pastList)

    override def reversed(): PowerIterator[A] = PowerIteratorImpl(listToStream(_pastList))
  }
}

object StreamExtension {
  import Lists._
  import Stream._
  def listToStream[A](l: List[A]): Stream[A] = l match{
    case List.Cons(h, t) => Stream.cons( h, listToStream(t))
  }
}
