package u04lab.code

object List {
  def apply[A](elems: A*): Lists.List[A] = {
    var _list: Lists.List[A] = Lists.List.Nil()
    for (i <- elems) {
      _list = Lists.List.append(_list, Lists.List.Cons(i, Lists.List.Nil()))
    }
    _list
  }
}

object ListFactoryTest extends App {
  val c1 = Course("PPS","Viroli")
  val c2 = Course("PCD","Ricci")
  val c3 = Course("SDR","D'Angelo")
  val courses = List(c1, c2, c3)
  courses match {
    case SameTeacher(t) => println(s"$courses have same teacher $t")
    case _ => println(s"$courses have diffent teacher")
  }
}


