package u03

import org.junit.Assert.{assertArrayEquals, assertEquals}
import org.junit.Test

import scala.annotation.tailrec


object Lists extends App {

  // A generic linkedlist
  sealed trait List[E]                                                  //Sum-Type

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]          //Product-Type
    case class Nil[E]() extends List[E]                                 //Product-Type

    //Metodi per costruire delle liste, che eventualmente posso usare con product type privati
    def empty[A](): List[A] = Nil() //Metodo per creare la lista vuota
    def of[A](a: A): List[A] = Cons(a, Nil())
    def of[A](a1: A, a2: A): List[A] = Cons(a1, Cons(a2, Nil()))
    def cons[A](h: A, t: List[A]): List[A] = Cons(h, t)

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def sumTail(l: List[Int]): Int = {
      @tailrec def _sum(list:List[Int], acc: Int): Int = list match {
        case Cons(h, t) => _sum(t, acc + h)
        case Nil() => acc
      }
      _sum(l,0)
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }
  }

  object TestList{
    import u03.Lists.List._

    val list1 = cons(10, cons(20, empty()))
    val emptyList = empty[Int]()

    val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
    val l2 = List.Cons(10, List.Cons(-20, List.Cons(30, List.Nil())))

    @Test def testEqualitiesOfConstruction(): Unit ={
      assertEquals(of(10), cons(10,empty()))
      assertEquals(of(10, 20), cons(10, cons(20, empty())))
    }

    @Test def testSum(): Unit ={
      assertEquals(60, sum(l))
      assertEquals(0, sum(emptyList))
    }

    @Test def testSumTail(): Unit ={
      assertEquals(60, sumTail(l))
      assertEquals(0, sumTail(emptyList))
    }

    @Test def testFilter(): Unit = {
      assertEquals(l, filter(l)(_ > 0))
      assertEquals(cons(10, cons(30, List.Nil())), filter(l2)(_ > 0))
      assertEquals(emptyList, filter(emptyList)(_ > 0))
      assertEquals(of(-20), filter(l2)(_ < 0))
      assertEquals(of("a"), filter(of("a", ""))(_ != ""))
    }

  }


  //println(sum(map(filter(l)(_>=20))(_+1))) // 21+31 = 52
}
